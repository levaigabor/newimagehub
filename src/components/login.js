import React, {Component} from "react";
import {
    Redirect,
} from "react-router-dom";

import fakeAuth from '../shared/fakeAuth';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';

import logo from '../assets/images/logo.png';


class Login extends Component {
    state = { redirectToReferrer: false };

    login = (event) => {
        fakeAuth.authenticate(() => {
            this.setState({ redirectToReferrer: true });
        });

        event.preventDefault();
    };



    render() {
        let { from } = this.props.location.state || { from: { pathname: "/" } };
        let { redirectToReferrer } = this.state;

        if (redirectToReferrer) return <Redirect to={from} />;

        return (
            <div className="container">
                <div className="blur"></div>
                <Card variant="outlined" className="card--login">
                    <div className="logo">
                        <img src={logo} />
                    </div>
                    <CardContent>
                        <p>Üdvözlünk az ImageHub oldalan. A bejelentkezéshez használd a Facebookodat!
                        </p>
                    </CardContent>
                    <CardActions>
                        <Button variant="outlined" className="card--login__button--login" color="primary" onClick={this.login}>Bejelentkezés</Button>
                    </CardActions>
                </Card>
            </div>
        );
    }
}

export default Login;