import React from "react";
import Button from "@material-ui/core/Button";
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import CircularProgress from '@material-ui/core/CircularProgress';
import Backdrop from '@material-ui/core/Backdrop';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function Upload() {

    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    return (
        <div className='container'>
            <div className='upload__wrapper'>
                <div className='image-placeholder'>
                    <p>Kattints ide a fotó hozzáadásához</p>
                    <AddAPhotoIcon />
                    {/*<div className='progress-bar'>*/}
                    {/*    <CircularProgress />*/}
                    {/*</div>*/}
                    <Backdrop open={open} onClick={handleClose}>
                        <CircularProgress color="inherit" />
                    </Backdrop>
                </div>
                <Button variant="outlined" className='button--upload-photo' color="primary" onClick={handleClick()}>Feltöltés</Button>
            </div>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success">
                    This is a success message!
                </Alert>
            </Snackbar>
        </div>
    )
}