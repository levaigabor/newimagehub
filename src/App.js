import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import { createMuiTheme } from '@material-ui/core/styles';
import fakeAuth from './shared/fakeAuth';
import { ThemeProvider } from '@material-ui/core/styles';
import Login from './components/login';
import './App.css';

import PrimarySearchAppBar from './shared/navigation'
import { blue } from "@material-ui/core/colors";
import Upload from "./components/upload";

const styles = {};

styles.fill = {
  position: "relative",
  height: "100%",
  width: "100%"
};

styles.content = {
  ...styles.fill
};

function App() {

  const theme = createMuiTheme({
    palette: {
      primary: blue,
    },
  });

  return (
      <div className="App">
        <ThemeProvider theme={theme}>
          <Router>
            <Route render={({ location }) => (
                <div style={styles.fill}>
                  <Route
                      exact
                      path = "/"
                      render = {() => <Redirect to='/login' />}
                  />
                  {fakeAuth.isAuthenticated ? <PrimarySearchAppBar/>  : ''}

                  <div style={styles.content}>
                    <TransitionGroup>
                      <CSSTransition
                          key = {location.key}
                          classNames="fade"
                          timeout={300}
                      >
                        <Switch location = {location}>
                          <Route path="/login" exact component={Login} />
                          <Route path="/upload" exact component={Upload} />
                          <Route component={NotFound} />
                        </Switch>
                      </CSSTransition>
                    </TransitionGroup>
                  </div>
                </div>
            )} />
          </Router>
        </ThemeProvider>
      </div>
  );
}

function NotFound() {
  return <h3>404</h3>
}

function PrivateRoute({ component: Component, ...rest }) {
  return (
      <Route
          {...rest}
          render={props =>
              fakeAuth.isAuthenticated ? (
                  <Component {...props} />
              ) : (
                  <Redirect
                      to={{
                        pathname: "/login",
                        state: { from: props.location }
                      }}
                  />
              )
          }
      />
  );
}

export default App;
